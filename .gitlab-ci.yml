workflow:
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: always
    - if: $CI_COMMIT_BRANCH
      changes:
        paths:
        - '*'
        - '**/*'
        compare_to: refs/heads/main
      when: always

stages:
  - detect_changes
  - pre_tasks
  - check_telegraf_installed
  - configure_telegraf
  - check_logs
  - merge_request_job

variables:
  BUILD_DIR_STORM: telegraf_storm
  BUILD_DIR_LOGON: telegraf_logon
  BUILD_PLUGINS: plugins
  ARTIFACTORY_PLUGINS: plugins/
  ARTIFACTORY_URL: /optimus-local/telegraf_plugins/
  GITLAB_URL: gitlab.com
  TELEGRAF_PATH: /etc/telegraf/
  LOG_TELEGRAF: /var/log/telegraf/telegraf.log
  ARTIFACTORY_STORM: plugins/storm/plugins.d/
  ARTIFACTORY_LOGON: plugins/logon/plugins.d/

pre_tasks:
  stage: pre_tasks
  script:
    - |
      jf --version
  except:
    - main
  only:
    changes:
      - plugins/**/*

check_telegraf_installed:
  stage: check_telegraf_installed
  script:
    - telegraf --version
    - test -d $TELEGRAF_PATH
    - ls -l | grep telegraf.conf
  except:
    - main
  tags:
    - k8s-runner
  only:
    changes:
      - plugins/**/*

configure_telegraf:
  stage: configure_telegraf
  script:
    - CHANGED_FILES_FULL_PATH=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA)
    - CHANGED_FILES=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | sed -E 's|^(.*?)/[^/]+?$|\1|')
    - mkdir $BUILD_DIR_STORM
    - mkdir $BUILD_DIR_LOGON
    - cp telegraf.conf $TELEGRAF_PATH/telegraf.conf
    - cp $CHANGED_FILES_FULL_PATH /etc/telegraf/
    - |
      #!/bin/bash
      IFS=$'\n' # Set the internal field separator to newline
      for full_path in $CHANGED_FILES_FULL_PATH; do
        dir_path=$(dirname "$full_path")
        echo $dir_path
        if [[ "$dir_path" == "plugins/storm/plugins.d" ]]; then
          echo "Handling changes in $dir_path"
          cp -r "$full_path" "$TELEGRAF_PATH/telegraf.d" && sleep 5
          service telegraf stop && sleep 5
          service telegraf start && sleep 5 
          service telegraf restart && sleep 10
          service telegraf status && sleep 30
          cp "$LOG_TELEGRAF" "$BUILD_DIR_STORM/"
          cp -r "$full_path" "$BUILD_DIR_STORM/" && sleep 5
        elif [[ "$dir_path" == "plugins/logon/plugins.d" ]]; then
          echo "Handling changes in $dir_path for logon"
          cp -r "$full_path" "$TELEGRAF_PATH/telegraf.d" && sleep 5
          service telegraf stop && sleep 5
          service telegraf start && sleep 5 
          service telegraf restart && sleep 10
          service telegraf status && sleep 30
          cp "$LOG_TELEGRAF" "$BUILD_DIR_LOGON/"
          cp -r "$full_path" "$BUILD_DIR_LOGON/" && sleep 5
        fi
      done
  except:
    - main
  artifacts:
    paths:
      - $BUILD_DIR_STORM
      - $BUILD_DIR_LOGON
  only:
    changes:
      - plugins/**/*
      
check_logs:
  stage: check_logs
  script:
    - |
      #!/bin/bash 
      error_found=false
      for dir in "$BUILD_DIR_STORM" "$BUILD_DIR_LOGON"; do
      if [[ -d "$dir" ]]; then
        log_files=("$dir"/*.log)

        if [ ${#log_files[@]} -gt 0 ]; then
          for log_file in "${log_files[@]}"; do
            if grep -qi "error" "$log_file"; then
              echo "Error found in $log_file"
              error_found=true
            fi
          done
        fi
      fi
      done

      if [ "$error_found" = true ]; then
        exit 1
      else
        echo "No errors found in any log files"
      fi
  dependencies:
    - configure_telegraf
  except:
    - main
  only:
    changes:
      - plugins/**/*

merge_request_job:
  stage: merge_request_job
  script:

    - jf rt u  $ARTIFACTORY_STORM $ARTIFACTORY_URL --target-props 'env=production'
    - jf rt u  $ARTIFACTORY_LOGON $ARTIFACTORY_URL --target-props 'env=production'
  only:
    - merge_requests
  dependencies:
    - configure_telegraf
